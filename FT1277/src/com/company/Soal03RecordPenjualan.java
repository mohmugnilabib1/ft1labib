package com.company;

import java.util.Scanner;
import java.util.TreeMap;

public class Soal03RecordPenjualan {
    public static void main(String[] args) {
        Scanner print = new Scanner(System.in);
        System.out.println("Masukan record penjualan: ");
        String string = print.nextLine();
        String[] stringSplit = string.split(", ");

        TreeMap<String, Integer> items = new TreeMap<>();

        System.out.println("record penjualan: ");
        for (int i = 0; i < stringSplit.length; i++) {
            String[] splitLagi = stringSplit[i].split(":");

            String key = splitLagi[0];
            int value = Integer.parseInt(splitLagi[1]);
            if (items.containsKey(key)) {
                items.put(key, value + items.get(key));
            } else {
                items.put(key,value);
            }

        }



        System.out.println("Summary record penjualan: ");
        for (String i : items.keySet()) {

            System.out.println( items.higherKey(i) + " : " + items.get(i));
        }




    }
}
