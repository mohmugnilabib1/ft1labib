package com.company;

import java.util.Scanner;

public class Soal04KonversiWaktu {
    public static void main(String[] args) {
        Scanner print = new Scanner(System.in);

        System.out.println("Masukan format jam: ");
        String jam = print.nextLine();

        int hh = Integer.parseInt(jam.substring(0,2));

        if(jam.length() > 5) {
            if (jam.charAt(6) == 'A') {
                if (hh == 12) {
                    System.out.print("00");
                    System.out.println(jam.substring(2,5));
                } else if(hh > 12){
                    hh -= 12;
                    System.out.println(hh);
                    System.out.println(jam.substring(2,5));
                }
                else {
                    System.out.print(hh);
                    System.out.println(jam.substring(2,5));
                }
            } else if (jam.charAt(6) == 'P') {
                if (hh == 12){
                    System.out.print("12");
                    System.out.println(jam.substring(2,5));
                }  else if(hh < 12){
                    hh += 12;
                    System.out.print(hh);
                    System.out.println(jam.substring(2,5));
                } else {
                    System.out.print(hh);
                    System.out.println(jam.substring(2,5));
                }

            }
        } else {
            if (hh >= 0 && hh <= 12) {
                if (hh == 0) {
                    hh += 12;
                    System.out.print(hh);
                    System.out.println(jam.substring(2, 5)+" AM");
                } else if (hh == 12) {
                    System.out.print(jam+" PM");
                } else if(hh < 12){
                    hh += 12;
                    System.out.println(hh);
                    System.out.println(jam.substring(2,5));
                }else {
                    System.out.print(jam+" AM");
                }
            } else {
                hh -= 12;
                System.out.print(hh);
                System.out.println(jam.substring(2, 5)+" PM");
            }
        }


    }
}

