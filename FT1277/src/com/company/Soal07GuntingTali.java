package com.company;

import java.util.Scanner;

public class Soal07GuntingTali {
    public static void main(String[] args) {
        Scanner print = new Scanner(System.in);

        System.out.print("z : ");
        int panjangTali = print.nextInt();
        int count = 0;

        System.out.print("x : ");
        int taliKecil = print.nextInt();

        if(panjangTali != taliKecil) {
            int potong = panjangTali;
            int counter = 0;

            while (counter < 1) {
                int potongSisa = 0;
                potongSisa = potong /2;
                count++;

                if (potongSisa == taliKecil ) {
                    counter++;
                } else {
                    potong = potongSisa;
                }
            }
        }

        System.out.println("Dipotong " + count + " kali");
    }
}
