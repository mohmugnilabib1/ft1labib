package com.company;

import java.util.Locale;
import java.util.Scanner;

public class Soal08enksirpsi {
    public static void main(String[] args) {
        Scanner print = new Scanner(System.in);
        System.out.print("Masukan String : ");
        String s = print.nextLine().toLowerCase(Locale.ROOT);

        System.out.println("Masukan n: ");
        int n = print.nextInt();


        int[] abjadOri = new int[26];
        System.out.print("Original alfabet: ");
        for (int i = 0; i < abjadOri.length; i++) {
            abjadOri[i] =  i;
            char c = (char) (i + 97);

            System.out.print(c);
        }
        System.out.println();
        System.out.print("Alfabet yang Dirotasi: ");
        String abjadEnkrip = "";
        for (int i = 0; i < abjadOri.length; i++) {
            char c = (char) ((char) abjadOri[i] + 97);
           char abjad = (char) (c + n);
            if (abjad > 122) {
                abjad = (char) (abjad - 122 + 97 - 1);
            }
            abjadEnkrip += abjad;
        }
        System.out.println(abjadEnkrip);

        String output="";
        System.out.println("hasil Enkripsi: ");
        for (int i = 0; i < s.length(); i++) {
            char ascii = s.charAt(i);
            if(ascii >= 97 && ascii <= 122) {
                ascii = (char) (ascii + n);
                if (ascii > 122) {
                    ascii = (char) (ascii - 122 + 97 - 1);
                }
                output += ascii;
            }


        }
        System.out.println(output);
    }
}
