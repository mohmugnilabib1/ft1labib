package com.company;

import java.util.Scanner;
//Berapa banyak angka fibonacci selain nol dibawah x yang merupakan angka genap
public class Soal09Fibonaci {
    public static void main(String[] args) {
        Scanner print = new Scanner(System.in);
        System.out.println("Masukan x : ");
        int n = print.nextInt();
        int count = 0;
        int awal = 1, akhir =1;
        int fibb = 1;
        for (int i = 0; i < 100; i++) {
            if ( awal < n && awal > 0){
                if (awal % 2 == 0){
                    System.out.print(awal + " ");
                    count++;
                }

            }

            fibb = awal + akhir;
            awal = akhir;
            akhir= fibb;
        }
        System.out.println();
        System.out.println("Banyaknya : " + count);
    }
}
