package com.company;

import java.util.Arrays;
import java.util.Locale;
import java.util.Scanner;

public class Soal10SampleCase {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.print("Input Kata: ");
        String data = input.nextLine().toLowerCase(Locale.ROOT);        //input diubah ke huruf kecil

        String hasilOutputVokal ="";
        String hasilOutputConsonant = "";


        for (int i = 0; i < data.length(); i++) {
            if (data.charAt(i) == 'a' || data.charAt(i) == 'e' || data.charAt(i)
                    == 'i' || data.charAt(i) == 'o' || data.charAt(i) == 'u') {
                hasilOutputVokal += data.charAt(i);
            }
        }

        //sortString
        char[] tempArrayVokal = hasilOutputVokal.toCharArray();
        Arrays.sort(tempArrayVokal);
        String hasilStringVokal = String.copyValueOf(tempArrayVokal);
        System.out.println("Vowel : "+hasilStringVokal);

        String reverseVowels = "";
        for (int i = hasilStringVokal.length() -1; i >=  0; i--) {
            char c = hasilStringVokal.charAt(i);
            reverseVowels +=c;
        }

        System.out.println("reserve vowel: "+ reverseVowels);

        //untuk mencari konsonan
        for (int i = 0; i < data.length(); i++) {
            char c = data.charAt(i);
            if (c >= 97 && c<= 122){
                if (data.charAt(i) != 'a' && data.charAt(i) != 'e' && data.charAt(i)
                        != 'i' && data.charAt(i) != 'o' && data.charAt(i) != 'u') {
                    hasilOutputConsonant += data.charAt(i);
                }
            }

        }
        //mengurutkan hasil dari konsonan
        char[] tempArrayConsonant = hasilOutputConsonant.toCharArray();
        Arrays.sort(tempArrayConsonant);
        String hasilStringConsonant = String.copyValueOf(tempArrayConsonant);
        System.out.println("Consonant : "+hasilStringConsonant);

        String reverseConsonant = "";
        for (int i = hasilStringConsonant.length() -1; i >=  0; i--) {
            char c = hasilStringConsonant.charAt(i);
            reverseConsonant +=c;
        }

        System.out.println("reserve Consonant: "+ reverseConsonant);

    }
}
